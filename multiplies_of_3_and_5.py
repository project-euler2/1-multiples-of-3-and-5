def multiplies_of_3_and_5(limit_number):
    """
    Input a number and you will have the sum of the natural numbers
    that can be divided by 3 or 5 below that number
    """
    result = 0
    for i in range(0, limit_number):
        if ((i % 3)  == 0) or ((i%5) == 0):
            result += i
    return result

print(multiplies_of_3_and_5(1000))